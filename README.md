Base Wordpress Template
=======================

All themes, plugins and uploads live in `httpdocs/wp-content`. Wordpress core is installed into `httpdocs/wp`.

Wordpress and any plugins can be installed/updated via composer (wpackagist is used for plugin install).

```
$ curl -sS https://getcomposer.org/installer | php
$ php composer.phar install
```

Make sure your composer.json is looking for the latest Wordpress

Setup
-----

Checkout a copy of this template as your base site and setup an appropriate apache config.

The first visit to the site will run you through the standard Wordpress install process, following which you should log in and activate the "default" theme.

Following that you're ready to start customising your templates.


Plugins and Licenced Software
-----------------------------

Refer to the `composer.json` file for Wordpress version and installed plugins. Any further private plugins should be outlined below.

