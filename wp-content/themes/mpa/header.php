<!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo('charset'); ?>" />
  <meta http-equiv="x-ua-compatible" content="ie=edge" />
  <title><?php wp_title('&raquo;','true','right'); ?><?php bloginfo('name'); ?></title>
  <meta property="og:image" content="" />
  <meta name="robots" content="noindex, nofollow">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
  <link href="<?php echo get_template_directory_uri(); ?>/favicon.png" type="image/x-icon" rel="shortcut icon"/>

  <?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>

<!--[if lt IE 9]>
  <div class="legacy-browser">
    <div class="legacy-browser-text">
      <img src="<?php echo get_template_directory_uri() ?>/media/email-logo.png" width="400" height="38" />
      <p>You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/?locale=en">upgrade your browser</a> to view our website properly.</p>
    </div>
  </div>
<![endif]-->

<header id="global-header">
  <?php if(is_front_page()) : ?>
    <video preload="none" muted autoplay loop width="1920" height="1080" poster="">
    <source src="<?php echo get_template_directory_uri(); ?>/media/home-hero-loop.mp4" type="video/mp4">
  </video>
  <?php endif; ?>
  <div class="inner">
    <div class="container">
      <header>
        <a href="/" class="logo"><img src="<?php echo get_template_directory_uri() ?>/media/logo-white.svg" alt="My Perfect Agent" /></a>
        <?php wp_nav_menu(
          array(
          'theme_location'  => 'primary',
          'menu'            => '',
          'container'       => 'nav',
          'container_class' => 'global-navigation'
          )
        ); ?>

      </header>

      <?php if(is_front_page()) : ?>
        <div id="home-intro">
          <h1>Thinking of Selling Your Home<br/> or Investment Property?</h1>
          <p>Use our tailored &amp; personalised service to find the perfect agent for you!</p>
          <a class="button" href="/get-started">Find My Perfect Agent</a>
        </div>
      <?php elseif (is_page('get-started'))  : ?>
        <div class="get-started-intro">
          <h1 class="title">Find Your Perfect Agent.</h1>
          <p>Agents will usually respond within 24 hours. You’ll receive responses online<br/> detailing their commissions, fees, and marketing strategies for your property.</p>
        </div>
      <?php endif; ?>

    </div>
  </div>
</header>
