<?php
/**
 * Template for displaying 404 pages (Not Found)
 *
 * @package Bootstrap Canvas WP
 * @since Bootstrap Canvas WP 1.0
 */

	get_header(); ?>

    <section class="error-page">
    	<div class="container">
				<h2><?php _e( 'This is somewhat embarrassing, isn&rsquo;t it?', 'bootstrapcanvaswp' ); ?></h2>
				<p>It seems we can&rsquo;t find what you&rsquo;re looking for. <a href="/">Click here</a> to return to the homepage.</p>
    	</div>
    </section>

	<?php get_footer(); ?>
