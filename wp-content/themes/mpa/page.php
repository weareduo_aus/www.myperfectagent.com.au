<?php
/**
 * Template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Bootstrap Canvas WP
 * @since Bootstrap Canvas WP 1.0
 */

	get_header(); ?>

	<main>

    <section class="basic-page">
        <div class="container">

					<?php if ( is_page('thank-you') ) : ?>
						<div class="thank-you-thumbs-up">
							<img src="<?php echo get_template_directory_uri(); ?>/media/thank-you-thumbs-up.svg" alt="Thank You!" />
						</div>
					<?php endif; ?>

					<h1><?php the_title(); ?></h1>

					<?php
					// Start the loop.
					while ( have_posts() ) : the_post();

						// Include the page content template.
						get_template_part( 'content', 'page' );

						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;

					// End the loop.
					endwhile;
					?>

        </div>
    </section>

	<?php get_footer(); ?>
