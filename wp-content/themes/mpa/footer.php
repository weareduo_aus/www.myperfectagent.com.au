<?php
/**
* Template for displaying the footer
*/
?>

</main>

<footer id="global-footer">
  <div class="container">
    <div class="row">
      <?php wp_nav_menu(
        array(
        'theme_location'  => 'footer',
        'menu'            => '',
        'container'       => 'nav',
        'container_class' => 'global-navigation'
        )
      ); ?>
      <p>My perfect agent takes the stress and time out of finding a real estate agent that is perfect for you. We know the right questions to ask and the qualities to looks for in an agent. With such an important decision we want to ensure you have as much information at your disposal as possible and help to stop you from making the easy mistakes when choosing an agent!</p>
    </div>
    <div class="row credit">
      <div class="footer-logo">
        <img src="<?php echo get_template_directory_uri(); ?>/media/logo-footer.svg" alt="MyPerfectAgent.">
      </div>
      <ul>
        <li>&copy; <?php echo date('Y'); ?> MyPerfectAgent</li>
        <li><span>|</span></li>
        <li>Built by <a href="http://weareduo.com.au" target="_blank"><strong>duo</strong></a></li>
      </ul>
    </div>
  </div>
</footer>

<?php include(TEMPLATEPATH . '/inc/section-contact.php'); ?>

<?php wp_footer(); ?>

</body>
</html>
