/**
 * Smoothly scrolls the window to an element.
 * @see     https://github.com/alicelieutier/smoothScroll
 * @version 0.1.1
 * @author  Jayden Seric
 */
window.smoothScrollTo = (function() {

  /**
   * Gets the offset of an element from the top of the page.
   * @param   {HTMLElement} element - An element.
   * @returns {number}      The offset in pixels.
   */
  function getTop(element) {
    if (element.nodeName === 'HTML') return -window.pageYOffset
    return element.getBoundingClientRect().top + window.pageYOffset;
  }

  /**
   * An easing function.
   * @see http://blog.greweb.fr/2012/02/bezier-curve-based-easing-functions-from-concept-to-implementation
   */
  function easeInOutCubic(t) { return t < .5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1 }

  /**
   * Calculates the scroll position at a given scroll animation moment.
   * @param   {number} start    - Start scroll Y position.
   * @param   {number} end      - End scroll Y position.
   * @param   {number} elapsed  - Time since beginning the scroll animation in milliseconds.
   * @param   {number} duration - Total scroll animation duration in milliseconds.
   * @returns {number} A scroll Y position.
   */
  function position(start, end, elapsed, duration) {
    if (elapsed > duration) return end;
    return start + (end - start) * easeInOutCubic(elapsed / duration);
  }

  /**
   * Smoothly scrolls the window to an element.
   * @param {Object}             options            - Options object.
   * @param {HTMLElement|number} options.target     - The target to scroll to, either an element or scroll Y position.
   * @param {number}             [options.duration] - Total scroll animation duration in milliseconds.
   * @param {function}           [options.callback] - A callback to run after scrolling is complete.
   */
  function smoothScroll(options) {
    // Options
    var target   = options.target,
        duration = options.duration || 760,
        callback = options.callback;
    // Derived
    var targetIsElement = target instanceof Element,
        scrollHeight    = document.body.scrollHeight,
        start           = window.pageYOffset,
        end             = targetIsElement ? getTop(target) : parseInt(target),
        time            = Date.now();
    function step() {
      var elapsed = Date.now() - time;
      window.scroll(0, position(start, end, elapsed, duration));
      if (elapsed < duration) requestAnimationFrame(step);
      else if (targetIsElement && scrollHeight !== document.body.scrollHeight) smoothScroll(options);
      else if (typeof callback === 'function') callback(target);
    }
    step();
  }

  // Expose the API
  return smoothScroll;

})();
