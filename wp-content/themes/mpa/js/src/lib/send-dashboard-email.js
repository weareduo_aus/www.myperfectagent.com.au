jQuery(function($) {
  $(document).ready(function(){
    $('#send-dashboard-email').click(function() {
      var post_id_number = $(this).data('id');
      var post_slug_url = $(this).data('dashboard-url');
      var post_user_email = $(this).data('user-email');
      var first_name = $(this).data('user-first-name');
      $.ajax({
        type:'GET',
        data:{
          action:'custom_mail',
          post_id: post_id_number,
          post_slug: post_slug_url,
          post_email: post_user_email,
          client_first_name: first_name
        },
        url: "/wp/wp-admin/admin-ajax.php",
        success: function(data) {
          alert('Dashboard email successfully sent.')
        },
        error: function(data) {
          alert('DASHBOARD EMAIL FAILED.')
        }
      });
    });
  });
});
