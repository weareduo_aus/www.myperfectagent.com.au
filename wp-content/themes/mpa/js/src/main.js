var body = $('body');
var getStartedForm = document.querySelector('#get-started');

document.addEventListener('DOMContentLoaded', function() {

  // Setup form skid
  document.queryAll('.skid').forEach(function(element) {
    // Initialize Skid slider
    var slider = new Skid.Slider({
      element: element
    });

    // Utilize URL hash
    Hurdler.hurdles.push({
      test     : function() { return this.id == 'get-started' },
      callback : function() { window.smoothScrollTo({ target: getStartedForm }) }
    });

    // Turn off navigation ability with dots
    slider.element.queryAll('> nav ol a').forEach(function(element) {
      element.addEventListener('click', function(element) {
        element.preventDefault();
      });
    });

    // Disable form submission on enter key press
    slider.element.addEventListener('keydown', function(e) {
      if (e.which == 13) {
        e.preventDefault();
        return false;
      }
    });

    // Handle start and end states
    slider.element.addEventListener('activated', function() {
      var submitButton = document.query('.frm_submit');
      // Start state
      var start = slider.activeSlide === slider.slides.firstElementChild;
      if (start) {
        submitButton.classList.add('hidden');
        slider.priorLink.classList.add('hidden');
        slider.element.classList.add('start');
      } else {
        slider.element.classList.remove('start');
        slider.priorLink.classList.remove('hidden');
      }
      // End state
      var end = slider.activeSlide === slider.slides.lastElementChild;
      if (end) {
        slider.nextLink.classList.add('hidden');
        slider.nextLink.style.display = "none";
        submitButton.classList.remove('hidden');
        slider.element.classList.add('end');
      } else {
        slider.nextLink.classList.remove('hidden');
        slider.nextLink.style.display = "block";
        submitButton.classList.add('hidden');
        slider.element.classList.remove('end');
      }
    });

    // Form validation

    var field_propertyType     = document.query('#field_property-type');
        field_address          = document.query('#field_address');
        field_suburb           = document.query('#field_suburb');
        field_postcode         = document.query('#field_postcode');
        field_state            = document.query('#field_state');
        field_neworestablished = document.query('#field_new-or-established-property');

        value_propertyType     = document.query('#field_property-type-value');
        value_numberofBedrooms = document.query('#field_number-of-bedrooms-value');
        value_suburb           = document.query('#field_suburb-value');
        value_postcode         = document.query('#field_postcode-value');
        value_state            = document.query('#field_state-value');
        value_neworestablished = document.query('#field_new-or-established-property-value');

        nextButton             = document.query('.skid nav .next');
        priorButton            = document.query('.skid nav .prior');
        submitButton           = document.query('.frm_submit input[type="submit"]');

        // Property Type
        field_propertyType.addEventListener('change', function() {
          var self = this;
          value_propertyType.value = self.value;
        });

        // State
        field_state.addEventListener('change', function() {
          var self = this;
          value_state.value = self.value;
        });

        // New or Established Property
        field_neworestablished.addEventListener('change', function() {
          var self = this;
          value_neworestablished.value = self.value;
        });

        // Custom next Link behavior
        slider.nextLink.addEventListener('click', function(element) {
          // Step 1
          if (slider.activeSlide.id == 'first') {
            var stepFields  = slider.element.queryAll('[data-validation-step-1]');
                errorMessage = document.query('.error-message');
            stepFields.forEach(function(value) {
              if (value.value == '') {
                element.preventDefault();
                document.body.classList.add('locked');
                errorMessage.classList.add('active');
                setTimeout(function() {
                  errorMessage.classList.remove('active');
                  document.body.classList.remove('locked');
                }, 2000);
              }
            });
          }

            // Step 2
            if (slider.activeSlide.id == 'second') {
              var stepFields  = slider.element.queryAll('[data-validation-step-2]');
                  errorMessage = document.query('.error-message');
              stepFields.forEach(function(value) {
                if (value.value == '') {
                  element.preventDefault();
                  document.body.classList.add('locked');
                  errorMessage.classList.add('active');
                  setTimeout(function() {
                    errorMessage.classList.remove('active');
                    document.body.classList.remove('locked');
                  }, 2000);
                }
              });
            }

          // Step 3
          if (slider.activeSlide.id == 'third') {
            var stepFields  = slider.element.queryAll('[data-validation-step-3]');
                errorMessage = document.query('.error-message');
            stepFields.forEach(function(value) {
              if (value.value == '') {
                element.preventDefault();
                document.body.classList.add('locked');
                errorMessage.classList.add('active');
                setTimeout(function() {
                  errorMessage.classList.remove('active');
                  document.body.classList.remove('locked');
                }, 2000);
              }
            });
          }

          // Step 4
          if (slider.activeSlide.id == 'fourth') {
            var stepFields  = slider.element.queryAll('[data-validation-step-4]');
                errorMessage = document.query('.error-message');
            stepFields.forEach(function(value) {
              if (value.value == '') {
                element.preventDefault();
                document.body.classList.add('locked');
                errorMessage.classList.add('active');
                setTimeout(function() {
                  errorMessage.classList.remove('active');
                  document.body.classList.remove('locked');
                }, 2000);
              }
            });
          }
      });

      // Hijack submit button and run validation before submit

      submitButton.addEventListener('click', function(e) {
        if (slider.activeSlide.id == 'fifth') {
          var stepFields         = slider.element.queryAll('[data-validation-step-5]'),
              userAgreementField = slider.element.query('#user-agreement-checkbox input[type="checkbox"]'),
              errorMessage = document.query('.error-message');
              submittingMessage = document.query('.submitting-form');
          stepFields.forEach(function(value) {
            if (value.value == '') {
              e.preventDefault();
              document.body.classList.add('locked');
              errorMessage.classList.add('active');
              setTimeout(function() {
                errorMessage.classList.remove('active');
                document.body.classList.remove('locked');
              }, 2000);
            }
          });
          if (userAgreementField.checked) {
            document.body.classList.add('locked');
            errorMessage.classList.remove('active');
            submittingMessage.classList.add('active');
          } else {
            e.preventDefault();
            document.body.classList.add('locked');
            errorMessage.classList.add('active');
            setTimeout(function() {
              errorMessage.classList.remove('active');
              document.body.classList.remove('locked');
            }, 2000);
          }
        }
      });

      function resizeSlides() {
        var element = document.query('.slides');
        element.queryAll('li.slide').forEach( function(slide) {
          slide.style.height = 'auto';
          slide.style.height = element.query('.active').offsetHeight + 'px';
        });
        element.style.height = element.query('.active').offsetHeight + 'px';
      }

      window.addEventListener('resize', resizeSlides, false);

      // Custom Skid Nav
      document.queryAll('li.slide').forEach(function(element, i) {
        element.addEventListener('active', function(event) {
          resizeSlides();
        });
      });

      resizeSlides();

  });

  if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ){
    document.body.classList.add('touch');
  }

  $('.contact-us-trigger a').on('click', function(e) {
      e.preventDefault();
      $('body').toggleClass('locked');
      $('.overlay.contact').css('display', 'flex');
      setTimeout(function() {
        $('.overlay.contact').addClass('active');
      }, 500);
  })

  $('.overlay.contact .close').on('click', function(e) {
      e.preventDefault();
      $('body').toggleClass('locked');
      $('.overlay.contact').removeClass('active');
      setTimeout(function() {
        $('.overlay.contact').css('display', 'none');
      }, 500);
  });

  $('.contact-me-button').click(function(e) {
    e.preventDefault();
    var self = $(this);
    var agentName = $(this).data('agent-name');
    var clientName = $(this).data('client-name');
    self.find('span').text('Notifying Agent');
    self.addClass('notifying');
    $.ajax({
      type:'POST',
      data:{
        action:'agent_mail',
        agent_name: agentName,
        client_name: clientName
      },
      url: "/wp/wp-admin/admin-ajax.php",
      success: function(data) {
        self.find('span').text('Agent Notified');
        self.addClass('notified');
      }
    });
  });

  // Run Hurdler
  Hurdler.run();

});
