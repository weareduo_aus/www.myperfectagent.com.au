<?php
/**
* Template Name: Get Started
*/

get_header(); ?>

<main>

<section id="get-started">
  <div class="container">

    <?php echo FrmFormsController::get_form_shortcode( array( 'id' => 2, 'title' => false, 'description' => false ) ); ?>

    <div class="error-message">
      <div class="message">
        <!-- <svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo get_template_directory_uri(); ?>/icons/graphics.svg#error-message"></use></svg> -->
        <h2>You must fill in all fields before proceeding.</h2>
      </div>
    </div>

    <div class="submitting-form">
      <div class="message">
        <div class="loading-spinner">
          <img src="<?php echo get_template_directory_uri(); ?>/media/form-submitting.svg" alt="" />
        </div>
        <h2>Submitting your details to My Perfect Agent...</h2>
      </div>
    </div>

  </div>
</section>

<?php get_footer(); ?>
