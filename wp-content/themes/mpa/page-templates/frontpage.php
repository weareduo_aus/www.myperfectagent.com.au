<?php
/**
* Template Name: Home
*/

get_header(); ?>

<main>

<section id="tagline">
  <h1>My perfect agent takes the stress and time out of finding a real estate agent that is perfect for you. We know the right questions to ask and the qualities to looks for in an agent. With such an important decision we want to ensure you have as much information at your disposal as possible and help to stop you from making the easy mistakes when choosing an agent!</h1>
</section>

<section id="how-it-works">
  <div class="container">
    <div class="intro">
      <h1 class="title">How it works.</h1>
      <p>MyPerfectAgent is a specialised service to help you find the perfect agent to assist in the sale of your home.</p>
    </div>
    <div class="steps">
      <div class="step property-details">
        <div class="icon"></div>
        <h5>1. Enter Property Details</h5>
        <p>Fill in some quick and specific details about your property, including age, rooms, land size and any extra comments you think are important, the more details the better!</p>
      </div>
      <div class="step tailored-search">
        <div class="icon"></div>
        <h5>2. Tailored Search</h5>
        <p>Using this information we search far and wide for a real estate expert that has experience selling similar properties to yours, we also look at any reviews made by the public, their overall experience in Real Estate, office location/reputation and much more!</p>
      </div>
      <div class="step myperfectagent">
        <div class="icon"></div>
        <h5>3. My Perfect Agent</h5>
        <p>Within 48 hours MPA will provide you with your own personalised and dedicated portal which gives you detailed recommendations on 3 Real Estate Experts. Based on our stringent & extensive search criteria we believe one of these superstars will be your perfect agent.</p>
      </div>
    </div>
  </div>
</section>

<section id="expert-agents">
  <div class="container">
    <div class="copy">
      <div class="inner">
        <h1 class="title">Expert Agents in Your Area.</h1>
        <p>Share some detailed information about your property & we will search out the most qualified agents for you and in particular your property!</p>
        <p>Using a very specialised & tailored search method specific to your property and the details provided, we’ll find an expert that has experience not only selling properties in your local area, but more importantly similar homes to yours, looking into the period of the home, build type, land size, and also agent reviews from previous clients just to name a few of some of our detailed criteria!</p>
        <p>Best of all it’s a free service! So don’t entrust just any agent with the sale of your biggest asset! As not all real estate agents are born equal!</p>
      </div>
    </div>
  </div>
</section>

<section id="faqs">
  <div class="container">
    <header>
      <h1>Frequently Asked Questions</h1>
      <p>Why you should use MyPerfectAgent and how it's free!</p>
    </header>
    <div class="faqs-wrapper">
      <div class="faq">
        <h3>How does MyPerfectAgent help?</h3>
        <p>MyPerfectAgent helps you dig deeper to find the agent that's right for you. Using a very specialised &amp; tailored search method specific to your property and the details provided, we’ll find an expert that has experience not only selling properties in your local area, but more importantly similar homes to yours, looking into the period of the home, build type, land size, and also agent reviews from previous clients just to name a few of some of our detailed criteria!</p>
      </div>
      <div class="faq">
        <h3>How is MyPerfectAgent free?</h3>
        <p>MyPerfectAgent is 100% free to you. There're no hidden catches, and you're under no obligation to select any agents that respond to your quote request. If you do successfully sell or rent with an agent through MyPerfectAgent, the agent pays an industry standard service fee, which is identical for every agent to maintain our impartiality, and your best interests.</p>
      </div>
      <div class="faq">
        <h3>How long do agents take to respond?</h3>
        <p>Agents only have 2 business days to respond to your service request. Most agents respond within hours, and proactive agents will respond immediately. The agents we have chosen for you will be displayed on your dashboard that you can revisit at any time with detailed information on why we chose them and their strengths. We feel that due to the specialised nature of the search one of these agents will be your perfect agent.</p>
      </div>
    </div>
  </div>
</section>

<section id="cta">
  <a href="/get-started" class="button">Find my perfect agent</a>
</section>

<?php get_footer(); ?>
