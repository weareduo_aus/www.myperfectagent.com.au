<?php
/**
 * Bootstrap Canvas WP functions and definitions
 *
 * Sets up the theme and provides some helper functions. Some helper functions
 * are used in the theme as custom template tags. Others are attached to action and
 * filter hooks in WordPress to change core functionality.
 *
 * The first function, bootstrapcanvaswp_setup(), sets up the theme by registering support
 * for various features in WordPress, such as post thumbnails, navigation menus, and the like.
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development and
 * http://codex.wordpress.org/Child_Themes), you can override certain functions
 * (those wrapped in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before the parent
 * theme's file, so the child theme functions would be used.
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are instead attached
 * to a filter or action hook. The hook can be removed by using remove_action() or
 * remove_filter() and you can attach your own function to the hook.
 *
 * We can remove the parent theme's hook only after it is attached, which means we need to
 * wait until setting up the child theme:
 *
 * <code>
 * add_action( 'after_setup_theme', 'my_child_theme_setup' );
 * function my_child_theme_setup() {
 *     // We are providing our own filter for excerpt_length (or using the unfiltered value)
 *     remove_filter( 'excerpt_length', 'bootstrapcanvaswp_excerpt_length' );
 *     ...
 * }
 * </code>
 *
 * For more information on hooks, actions, and filters, see http://codex.wordpress.org/Plugin_API.
 *
 * @package Bootstrap Canvas WP
 * @since Bootstrap Canvas WP 1.0
 */

/*
 * Set the content width based on the theme's design and stylesheet.
 *
 * Used to set the width of images and content. Should be equal to the width the theme
 * is designed for, generally via the style.css stylesheet.
 */
global $content_width;
if (!isset($content_width)) $content_width = 900;

/* Tell WordPress to run bootstrapcanvaswp_setup() when the 'after_setup_theme' hook is run. */
add_action('after_setup_theme', 'bootstrapcanvaswp_setup');

if (!function_exists('bootstrapcanvaswp_setup')):
    /**
     * Set up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which runs
     * before the init hook. The init hook is too late for some features, such as indicating
     * support post thumbnails.
     *
     * To override bootstrapcanvaswp_setup() in a child theme, add your own bootstrapcanvaswp_setup to your child theme's
     * functions.php file.
     *
     * @uses add_theme_support()        To add support for post thumbnails, custom headers and backgrounds, and automatic feed links.
     * @uses register_nav_menus()       To add support for navigation menus.
     * @uses add_editor_style()         To style the visual editor.
     * @uses load_theme_textdomain()    For translation/localization support.
     * @uses register_default_headers() To register the default custom header images provided with the theme.
     * @uses set_post_thumbnail_size()  To set a custom post thumbnail size.
     *
     * @since Bootstrap Canvas WP 1.0
     */
    function bootstrapcanvaswp_setup()
    {
        // This theme styles the visual editor with editor-style.css to match the theme style.
        add_editor_style('editor-style.css');

        // Post Format support.
        add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat'));

        // This theme uses post thumbnails
        add_theme_support('post-thumbnails');

        // Custom Image Sizes
//        add_image_size('exmplae', 700, 700, true);

        // Add default posts and comments RSS feed links to head
        add_theme_support('automatic-feed-links');

        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory
         */
        load_theme_textdomain('bootstrapcanvaswp', get_template_directory() . '/languages');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'primary' => __('Primary Menu', 'bootstrapcanvaswp')
        ));

        register_nav_menus(array(
            'footer' => __('Footer Menu', 'bootstrapcanvaswp')
        ));

        // This theme allows users to set a custom background.
        $args = array(
            // Let WordPress know what our default background color is.
            'default-color' => 'fff',
        );
        add_theme_support('custom-background', $args);

        // The custom header business starts here.

        $args = array(
            // The height and width of our custom header.
            'width' => '980',
            'height' => '170',
            // Support flexible widths and heights.
            'flex-height' => true,
            'flex-width' => true,
            // Let WordPress know what our default text color is.
            'default-text-color' => '333',
        );
        add_theme_support('custom-header', $args);

        // This feature allows themes to add document title tag to HTML <head>.
        add_theme_support('title-tag');
    }

    add_action('after_setup_theme', 'bootstrapcanvaswp_setup');
endif;

/**
 * Enqueue scripts and styles for front-end.
 *
 * @since Bootstrap Canvas WP 1.0
 */
function bootstrapcanvaswp_scripts()
{
    wp_enqueue_style('custom-styles', get_template_directory_uri() . '/css/main.css', '3.3.0');
    wp_enqueue_script('scripts-js', get_template_directory_uri() . '/js/main.min.js', array('jquery'), '', true);
}

add_action('wp_enqueue_scripts', 'bootstrapcanvaswp_scripts');

/**
 * Register widgetized areas, including main sidebar and three widget-ready columns in the footer.
 *
 * To override bootstrapcanvaswp_widgets_init() in a child theme, remove the action hook and add your own
 * function tied to the init hook.
 *
 * @since Bootstrap Canvas WP 1.0
 *
 * @uses register_sidebar()
 */
function bootstrapcanvaswp_widgets_init()
{
    // Area 1, located at the top of the sidebar.
    register_sidebar(array(
        'name' => __('Primary Widget Area', 'bootstrapcanvaswp'),
        'id' => 'primary-widget-area',
        'description' => __('Add widgets here to appear in your sidebar.', 'bootstrapcanvaswp'),
        'before_widget' => '<div id="%1$s" class="sidebar-module widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
    ));
}

add_action('widgets_init', 'bootstrapcanvaswp_widgets_init');

/**
 * Use get_the_excerpt() to print an excerpt by specifying a maximium number of characters.
 *
 * To override this length in a child theme, remove the filter and add your own
 * function tied to the excerpt_length filter hook.
 *
 * @since Bootstrap Canvas WP 1.0
 *
 * @param int $charlength The number of excerpt characters.
 * @return int The filtered number of excerpt characters.
 */
function the_excerpt_max_charlength($charlength)
{
    $excerpt = get_the_excerpt();
    $charlength++;

    if (mb_strlen($excerpt) > $charlength) {
        $subex = mb_substr($excerpt, 0, $charlength - 5);
        $exwords = explode(' ', $subex);
        $excut = -(mb_strlen($exwords[count($exwords) - 1]));
        if ($excut < 0) {
            echo mb_substr($subex, 0, $excut);
        } else {
            echo $subex;
        }
        echo '[...]';
    } else {
        echo $excerpt;
    }
}


/*** Word limit for excerpt
 *
 */

function get_excerpt($count)
{
    $permalink = get_permalink($post->ID);
    $excerpt = get_the_content();
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, $count);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    return $excerpt;
}

/**
 * Sanitize Customizer Font Selections
 *
 * @link http://codex.wordpress.org/Theme_Customization_API
 * @since Bootstrap Canvas WP 1.0
 */
function bootstrapcanvaswp_sanitize_font_selection($input)
{
    $valid = array(
        'arial, helvetica, sans-serif' => 'Arial',
        'arial black, gadget, sans-serif' => 'Arial Black',
        'comic sans ms, cursive, sans-serif' => 'Comic Sans MS',
        'courier new, courier, monospace' => 'Courier New',
        'georgia, serif' => 'Georgia',
        'impact, charcoal, sans-serif' => 'Impact',
        'lucida console, monaco, monospace' => 'Lucida Console',
        'lucida sans unicode, lucida grande, sans-serif' => 'Lucida Sans Unicode',
        'palatino linotype, book antiqua, palatino, serif' => 'Palatino Linotype',
        'tahoma, geneva, sans-serif' => 'Tahoma',
        'times new roman, times, serif' => 'Times New Roman',
        'trebuchet ms, helvetica, sans-serif' => 'Trebuchet MS',
        'verdana, geneva, sans-serif' => 'Verdana',
    );

    if (array_key_exists($input, $valid)) {
        return $input;
    } else {
        return '';
    }
}

/**
 * Sanitize Customizer Checkbox
 *
 * @link http://codex.wordpress.org/Theme_Customization_API
 * @since Bootstrap Canvas WP 1.0
 */
function bootstrapcanvaswp_sanitize_checkbox($input)
{
    if ($input == 1) {
        return 1;
    } else {
        return '';
    }
}

if (!function_exists('bootstrapcanvaswp_comment')) :
    /**
     * Template for comments and pingbacks.
     *
     * To override this walker in a child theme without modifying the comments template
     * simply create your own bootstrapcanvaswp_comment(), and that function will be used instead.
     *
     * Used as a callback by wp_list_comments() for displaying the comments.
     *
     * @since Bootstrap Canvas WP 1.0
     */
    function bootstrapcanvaswp_comment($comment, $args, $depth)
    {
        $GLOBALS['comment'] = $comment;
        switch ($comment->comment_type) :
            case 'pingback' :
            case 'trackback' :
                // Display trackbacks differently than normal comments.
                ?>
                <li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
                <p><?php _e('Pingback:', 'bootstrapcanvaswp'); ?> <?php comment_author_link(); ?> <?php edit_comment_link(__('Edit', 'bootstrapcanvaswp'), '<span class="comment-meta edit-link"><span class="glyphicon glyphicon-pencil"></span> ', '</span>'); ?></p>
                <?php
                break;
            default :
                // Proceed with normal comments.
                global $post;
                ?>
            <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
                <article id="comment-<?php comment_ID(); ?>" class="comment">
                    <header class="comment-meta comment-author vcard">
                        <?php
                        echo get_avatar($comment, 44);
                        printf(' <cite><b class="fn">%1$s</b> %2$s</cite>',
                            get_comment_author_link(),
                            // If current post author is also comment author, make it known visually.
                            ($comment->user_id === $post->post_author) ? '<span>' . __('Post author', 'bootstrapcanvaswp') . '</span>' : ''
                        );
                        printf('<span class="glyphicon glyphicon-calendar"></span> <a href="%1$s"><time datetime="%2$s">%3$s</time></a>',
                            esc_url(get_comment_link($comment->comment_ID)),
                            get_comment_time('c'),
                            /* translators: 1: date, 2: time */
                            sprintf(__('%1$s at %2$s', 'bootstrapcanvaswp'), get_comment_date(), get_comment_time())
                        );
                        ?>
                    </header>
                    <!-- .comment-meta -->

                    <?php if ('0' == $comment->comment_approved) : ?>
                        <p class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.', 'bootstrapcanvaswp'); ?></p>
                    <?php endif; ?>

                    <section class="comment-content comment">
                        <?php comment_text(); ?>
                        <?php edit_comment_link(__('Edit', 'bootstrapcanvaswp'), '<p class="comment-meta edit-link"><span class="glyphicon glyphicon-pencil"></span> ', '</p>'); ?>
                    </section>
                    <!-- .comment-content -->

                    <div class="reply">
                        <?php comment_reply_link(array_merge($args, array('reply_text' => __('Reply', 'bootstrapcanvaswp'), 'after' => ' <span>&darr;</span>', 'depth' => $depth, 'max_depth' => $args['max_depth']))); ?>
                    </div>
                    <!-- .reply -->
                    <hr/>
                </article>
                <!-- #comment-## -->
                <?php
                break;
        endswitch; // end comment_type check
    }
endif;


//Page Slug Body Class
function add_slug_body_class($classes)
{
    global $post;
    if (isset($post)) {
        $classes[] = $post->post_type . '-' . $post->post_name;
    }
    return $classes;
}

add_filter('body_class', 'add_slug_body_class');


// IS PAGE CHILD
function is_page_child($pid)
{// $pid = The ID of the page we're looking for pages underneath
    global $post;         // load details about this page
    $anc = get_post_ancestors($post->ID);
    foreach ($anc as $ancestor) {
        if (is_page() && $ancestor == $pid) {
            return true;
        }
    }
    if (is_page() && (is_page($pid)))
        return true;   // we're at the page or at a sub page
    else
        return false;  // we're elsewhere
}

;


/**
 * Add HTML5 data attributes to hidden fields on form.
 *
 */

function homepageSurveyModifyInput($field, $echo=true){
  $html = '';
  if (in_array($field['id'], array(95, 129, 130, 99, 102, 134))) {
     $html = 'data-validation-step-1';
  }

  if (in_array($field['id'], array(138, 139, 140, 141))) {
     $html = 'data-validation-step-2';
  }

  if (in_array($field['id'], array(114, 115, 116))) {
     $html = 'data-validation-step-3';
  }

  if (in_array($field['id'], array(119, 120, 121, 124))) {
     $html = 'data-validation-step-4';
  }

  if (in_array($field['id'], array(127))) {
     $html = 'data-validation-step-5';
  }

  if($echo)
    echo $html;
  return $html;
}
add_action('frm_field_input_html', 'homepageSurveyModifyInput');


/**
 * Send dashboard email to user.
 *
 */

add_action('media_buttons', 'add_send_dashboard_email');

function add_send_dashboard_email() {
  global $post;
  $title = get_the_title();
  $title_array = explode(' ', $title);
  $first_word = $title_array[0];
  $user_email = get_post_meta($post->ID, 'client_email', true);
  echo '<a href="#" id="send-dashboard-email" data-user-first-name="'.$first_word.'" data-user-email="'.$user_email.'" data-id="' . get_the_ID() . '" data-dashboard-url="' . get_permalink() . '" class="button">Send Dashboard email</a>';
}

function include_send_dashboard_email_js_file() {
    wp_enqueue_script('send_dashboard_email', '/../wp-content/themes/mpa/js/src/lib/send-dashboard-email.js');
}
add_action('wp_enqueue_media', 'include_send_dashboard_email_js_file');

function custom_mail() {
    $post_url = $_REQUEST['post_slug'];
    $user_email = $_REQUEST['post_email'];
    $client_first_name = $_REQUEST['client_first_name'];

  	// Sending an email using a template and merge vars
  	$to = $user_email;
  	$message = array(
  		'subject' => 'Your custom dashboard is ready!',
  		'from_name' => 'My Perfect Agent',
  		'from_email' => 'info@myperfectagent.com.au',
  		'to' => $to,
  		'html' => array(
				array('name' => 'main', 'content' => 'Here is your custom dashboard url <span style="color: #000000 !important; font-weight: bold; display: block;"><font color="#ef4347">' . $post_url . '</font></span>'),
				array('name' => 'header', 'content' => '<h1 style="margin: 0 0 30px; font-size: 24px; color: #ef4347; font-weight: normal; text-transform: capitalize;">Hi '.$client_first_name.'!</h1>'),
			)
  	);
  	wpMandrill::sendEmail(
  		$message,
      $tags = array('Tag 1', 'Tag 2'),
  		$template_name = 'Dashboard Email'
  	);

    // exit because it will error without
    exit;
}

add_action( 'wp_ajax_nopriv_custom_mail', 'custom_mail' );
add_action( 'wp_ajax_custom_mail', 'custom_mail' );

function agent_mail() {
    // this is where you should be able to pass email, subject, etc
    $data = $_POST['data'];
    $client_name = $_REQUEST['client_name'];
    $agent_name = $_REQUEST['agent_name'];
    $recipients = array(
      'admin@myperfectagent.com.au',
      'dayne@weareduo.com.au'
    );

    $message = '
  	<p>Hi Alex!</p>
    <p>A client of yours has made a selection as to who they would like to be their agent.</p>
  	<p><b>Client:</b> '. $client_name .'</p>
  	<p><b>Agent Name:</b> '. $agent_name .'</p>
    <p>Please inform the agent that they have been requested.</p>
  	';

    // send mail
    wp_mail($recipients,'New Agent Selection', $message);

    // echo just for testing, i log this to the console via the JS
    echo 'email sent';

    // exit because it will error without
    exit;
}

add_action( 'wp_ajax_nopriv_agent_mail', 'agent_mail' );
add_action( 'wp_ajax_agent_mail', 'agent_mail' );

add_action('frm_form_classes', 'frm_form_classes');
function frm_form_classes($form){
if($form->id == 2){//Change 25 to the ID of your form or remove this line (and the bottom bracket) if you want it to apply to all forms.
  echo 'main-form';
   }
}


// function SendTemplate() {
// 	// Create a template called MyCoolTemplate and use this code:
// 	$template_code = '
// 	Hello *|FNAME|*,
//
// 	<p>Your personal coupon code is: *|COUPON|*</p>
//
// 	<p>Event Date: *|DATE|*</p>
// 	<p>Address: *|ADDRESS|*</p>
//
// 	<div mc:edit="body"></div>
// 	<div mc:edit="sidebar"></div>
// 	';
//
// 	// Sending an email using a template and merge vars
// 	$to = 'dayne@weareduo.com.au';
// 	$globalmv = array(
// 					array('name' => 'date', 'content' => 'Tomorrow morning!'),
// 					array('name' => 'address', 'content' => 'Our office')
// 				);
// 	$mv	= array(
// 				array(
// 					'rcpt' => 'dayne@weareduo.com.au',
// 					'vars' => array(
// 									array('name' => 'fname', 'content' => 'Number One'),
// 									array('name' => 'coupon', 'content' => '123456'),
// 								)
// 				),
// 				array(
// 					'rcpt' => 'dayne@weareduo.com.au',
// 					'vars' => array(
// 									array('name' => 'fname', 'content' => 'Number Two'),
// 									array('name' => 'coupon', 'content' => '654321'),
// 								)
// 				),
// 			);
// 	$message = array(
// 		'subject' => 'Email Subject',
// 		'from_name' => 'Freedie',
// 		'from_email' => 'info@myperfectagent.com.au',
// 		'to' => $to,
// 		'merge' => true,
// 		'global_merge_vars' => $globalmv,
// 		'merge_vars' => $mv,
// 		'html' => array(
// 						array('name' => 'body', 'content' => 'This is the body!'),
// 						array('name' => 'sidebar', 'content' => 'This is the sidebar!'),
// 					)
// 	);
// 	wpMandrill::sendEmail(
// 		$message,
// 		$tags = array('Tag 1', 'Tag 2'),
// 		$template_name = 'Filling out Get Started form'
// 	);
// }

// function ga_tracking() {
//     echo   "<script>
//   (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
//   (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
//   m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
//   })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
//
//   ga('create', 'UA-66363944-2', 'auto');
//   ga('send', 'pageview');
//
// </script>";
// }
//
// add_action('wp_footer', 'ga_tracking');
