<?php
/**
 * The sidebar containing the main widget area
 *
 * If no active widgets are in the sidebar, hide it completely.
 *
 * @package Bootstrap Canvas WP
 * @since Bootstrap Canvas WP 1.0
 */
?>

<div class="<?php if ( is_singular() ) : ?><?php echo 'col-sm-3 col-sm-offset-2'; ?><?php else : ?><?php echo 'col-sm-4 col-md-3' ?><?php endif; ?> blog-sidebar">
    <div class="inner">
    <?php if ( ! dynamic_sidebar( 'primary-widget-area' ) ) : ?>
  </div>
</div>
<?php endif; ?>