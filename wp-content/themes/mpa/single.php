<?php
/**
 * Template for displaying all single posts
 *
 * @package Bootstrap Canvas WP
 * @since Bootstrap Canvas WP 1.0
 */

	get_header(); ?>

    <section class="page-header blog-header text-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <h1><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
    </section>

    <section class="blog standard-block-margin">
        <div class="container">
            <div class="row">

                <div class="col-sm-7 blog-main">

                    <?php get_template_part( 'loop', 'single' ); ?>

                </div>

                <?php get_sidebar(); ?>
            </div>
        </div>
    </section>

	<?php get_footer(); ?>
