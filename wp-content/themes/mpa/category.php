<?php
/**
 * Template for displaying Category Archive pages
 *
 * @package Bootstrap Canvas WP
 * @since Bootstrap Canvas WP 1.0
 */

	get_header(); ?>

    <section class="blog standard-block-padding bg">
        <div class="container">
            <div class="row">

                <div class="col-sm-9 blog-main">

                    <div class="blog-subsection-title">
                        <h2><?php printf( __( 'Category Archives: %s', 'bootstrapcanvaswp' ), '<span>' . single_cat_title( '', false ) . '</span>' ); ?></h2>
                    </div>
                    <hr />
                    <?php get_template_part( 'loop', 'category' ); ?>

                </div>

                <?php get_sidebar(); ?>
            </div>
        </div>
    </section>





      
	<?php get_footer(); ?>