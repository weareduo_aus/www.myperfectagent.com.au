<?php
/**
 * Custom Post Type
 * In the example below, we make a custom post type called product
 */
add_action( 'init', 'theme_post_type' );
function theme_post_type() {
    // Product
    $labels = array(
        'name' => 'Product',
        'singular_name' => 'Product',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Product',
        'edit_item' => 'Edit Product',
        'new_item' => 'New Product',
        'all_items' => 'All Products',
        'view_item' => 'View Product',
        'search_items' => 'Search Products',
        'not_found' =>  'No Products found',
        'not_found_in_trash' => 'No Products found in Trash',
        'parent_item_colon' => '',
        'menu_name' => 'Product'
    );
    
    $args = array(
        'labels' => $labels,
        'public' => true,
        'hierarchical' => false,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'products' ),
        'capability_type' => 'post',
        'has_archive' => false,
        'menu_position' => null,
        'supports' => array(
            'title',
            'editor',
            'page-attributes'
        ),
        'taxonomies'=>array()
    );
    register_post_type( 'product', $args);
}

/**
 * Custom Taxonomies
 * In the example below, we make a new taxonomy called "Custom Taxo" for the above Custom Post Type "Product"
 */
add_action( 'init', 'theme_taxonomy');
function theme_taxonomy() {

    // Product taxonomies
    $labels = array(
        'name'              => _x( 'Custom Taxo', 'taxonomy general name' ),
        'singular_name'     => _x( 'Custom Taxo', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Custom Taxo' ),
        'all_items'         => __( 'All Custom Taxos' ),
        'parent_item'       => __( 'Parent Custom Taxo' ),
        'parent_item_colon' => __( 'Parent Custom Taxo:' ),
        'edit_item'         => __( 'Edit Custom Taxo' ),
        'update_item'       => __( 'Update Custom Taxo' ),
        'add_new_item'      => __( 'Add New Custom Taxo' ),
        'new_item_name'     => __( 'New Custom Taxo Name' ),
        'menu_name'         => __( 'Custom Taxo' ),
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'custom-taxo','hierarchical' => true  )
    );

    register_taxonomy( 'custom_tax', array( 'product' ), $args );
}
