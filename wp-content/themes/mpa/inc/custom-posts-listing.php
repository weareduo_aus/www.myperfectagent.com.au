<section class="listing-block-wrapper <?php if ( is_page( 'work' ) ) : ?>office-spaces<?php endif; ?>">
    <?php if ( is_page( 'work' ) ) : ?>
        <h2>Available Office Space</h2>
    <?php endif; ?>
    <div class="row">

        <?php

        $args = array(
            'post_type' => $post_type_name,
            'posts_per_page' => -1
        );

        if ($post_type_name == 'event') {
            $sub_args = array(
                'meta_key' => 'start_date',
                'orderby' => 'meta_value',
                'order' => 'ASC',
                'meta_query' => array(
                    array(
                        'key' => 'start_date',
                        'value' => date('Ymd'),
                        'compare' => '>=',
                    )
                )

            );

            $args = array_merge($args, $sub_args);
        }

        $loop = new WP_Query( $args );

        ?>

        <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
            <?php switch($post_type_name){

                case "apartment": ?>
                    <div class="col-md-4">
                        <?php the_post_thumbnail(); ?>
                        <div class="listing-block ">
                            <p class="title"><?php the_title(); ?></p>
                            <?php $content = get_the_content();
                            $trimmed_content = wp_trim_words( $content, 15, '...' ); ?>
                            <p class="excerpt"><?php echo $trimmed_content; ?></p>
                            <div class="footer">
                                <a href="<?php the_permalink(); ?>">Read More<?php include(TEMPLATEPATH . '/inc/icon-gear.php'); ?></a>
                            </div>
                        </div>
                    </div>

                    <?php
                    break;

                case "attraction": ?>
                    <div class="col-md-4">
                        <?php the_post_thumbnail(); ?>
                        <div class="listing-block ">
                            <p class="title"><?php the_title(); ?></p>
                            <p class="when"><?php the_field('when'); ?></p>
                            <?php $content = get_the_content();
                            $trimmed_content = wp_trim_words( $content, 15, '...' ); ?>
                            <p class="excerpt"><?php echo $trimmed_content; ?></p>
                            <div class="footer">
                                <a href="<?php the_field('website_url'); ?>" target="_blank">Visit Website<?php include(TEMPLATEPATH . '/inc/icon-gear.php'); ?></a>
                            </div>
                        </div>
                    </div>

                    <?php
                    break;

                case "attraction": ?>
                    <div class="col-md-4">
                        <?php the_post_thumbnail(); ?>
                        <div class="listing-block ">
                            <p class="title"><?php the_title(); ?></p>
                            <p class="when"><?php the_field('when'); ?></p>
                            <p class="excerpt"><?php echo get_excerpt(90); ?>...</p>
                            <div class="footer">
                                <a href="<?php the_permalink(); ?>">Read More<?php include(TEMPLATEPATH . '/inc/icon-gear.php'); ?></a>
                            </div>
                        </div>
                    </div>

                    <?php
                    break;

                case "event":

                    $startDate = date('M d', strtotime(get_field('start_date')));
                    $endDate = date('M d', strtotime(get_field('end_date')));

                    ?>
                    <div class="col-md-4">
                        <?php the_post_thumbnail(); ?>
                        <div class="listing-block ">
                            <p class="title"><?php the_title(); ?></p>
                            <?php if($startDate): ?>
                                <p class="when"><?php echo $startDate?> - <?php echo $endDate; ?></p>
                            <?php endif; ?>
                            <?php $content = get_the_content();
                            $trimmed_content = wp_trim_words( $content, 15, '...' ); ?>
                            <p class="excerpt"><?php echo $trimmed_content; ?></p>
                            <div class="footer">
                                <a href="<?php the_permalink(); ?>">Read More<?php include(TEMPLATEPATH . '/inc/icon-gear.php'); ?></a>
                            </div>
                        </div>
                    </div>

                    <?php
                    break;

                case "hotel": ?>
                    <div class="col-md-4">
                        <?php the_post_thumbnail(); ?>
                        <div class="listing-block ">
                            <p class="title"><?php the_title(); ?></p>
                            <p class="when"><?php the_field('when'); ?></p>
                            <p class="excerpt"><?php echo get_excerpt(90); ?>...</p>
                            <div class="footer">
                                <a href="<?php the_permalink(); ?>">Visit Website<?php include(TEMPLATEPATH . '/inc/icon-gear.php'); ?></a>
                            </div>
                        </div>
                    </div>

                    <?php
                    break;

                case "office": ?>
                    <div class="col-md-4">
                        <?php the_post_thumbnail(); ?>
                        <div class="listing-block ">
                            <p class="title"><?php the_title(); ?></p>
                            <p class="when"><?php the_field('when'); ?></p>
                            <?php $content = get_the_content();
                            $trimmed_content = wp_trim_words( $content, 15, '...' ); ?>
                            <p class="excerpt"><?php echo $trimmed_content; ?></p>
                            <div class="footer">
                                <a href="<?php the_permalink(); ?>">Read More<?php include(TEMPLATEPATH . '/inc/icon-gear.php'); ?></a>
                            </div>
                        </div>
                    </div>

                    <?php
                    break;

                default: ?>
                <?php }

        endwhile; ?>

    </div>
</section>