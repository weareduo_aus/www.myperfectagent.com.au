<section class="overlay contact">
  <div class="contact-form">
    <div class="close"></div>
    <header>
      <h1>Contact Us</h1>
      <p>If you have a specific question or wish to contact us,<br/> please fill out the form below.</p>
    </header>
    <?php echo do_shortcode('[contact-form-7 id="62" title="Contact Form" html_id="default-contact-form"]'); ?>
  </div>
</section>
