<?php
/**
 * Template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Bootstrap Canvas WP
 * @since Bootstrap Canvas WP 1.0
 */

	get_header(); ?>

    <section class="blog standard-block-padding bg">
        <div class="container">
            <div class="row">

                <div class="col-sm-9 blog-main">

                    <div class="blog-subsection-title">
                        <h2>
                            <?php
                            if ( is_day() ) :
                                printf( __( 'Daily Archives: <span>%s</span>', 'bootstrapcanvaswp' ), get_the_date() );
                            elseif ( is_month() ) :
                                printf( __( 'Monthly Archives: <span>%s</span>', 'bootstrapcanvaswp' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'bootstrapcanvaswp' ) ) );
                            elseif ( is_year() ) :
                                printf( __( 'Yearly Archives: <span>%s</span>', 'bootstrapcanvaswp' ), get_the_date( _x( 'Y', 'yearly archives date format', 'bootstrapcanvaswp' ) ) );
                            else :
                                _e( 'Blog Archives', 'bootstrapcanvaswp' );
                            endif;
                            ?>
                        </h2>
                    </div>
                    <hr />
                    <?php get_template_part( 'loop', 'archive' ); ?>

                </div>

                <?php get_sidebar(); ?>
            </div>
        </div>
    </section>
      
	<?php get_footer(); ?>