<?php
/**
* Template for displaying all single posts
*
* @package Bootstrap Canvas WP
* @since Bootstrap Canvas WP 1.0
*/

get_header(); ?>

<section class="user-intro">
	<div class="inner">
		<div class="container">
			<h1 class="title">Hi <?php the_title(); ?>,</h1>
			<p>Welcome to you personally tailored My Perfect Agent dashboard!</p>
		</div>
	</div>
</section>

<main>

<section class="introduction">
	<div class="container">
		<h1>My Perfect Agent</h1>
		<p>Based on the information you have provided we have selected 3 super star real estate agents that we feel will be perfect for you and your property.</p>
	</div>
</section>

<section class="user-agents">
	<div class="container">

		<?php

		$post_object = get_field('agent_number_1');

		$title = get_the_title();

		if( $post_object ):

			// override $post
			$post = $post_object;
			setup_postdata( $post );

			?>
			<div class="agent" id="agent_1">
				<figure>
					<?php if( get_field('agent_photo') ): ?>
						<img src="<?php the_field('agent_photo'); ?>" alt="" />
					<?php else : ?>
						<img src="<?php echo get_template_directory_uri(); ?>/media/agent-placeholder.png" alt="Agent" />
					<?php endif; ?>
				</figure>
				<header>
					<h3><?php the_title(); ?></h3>
					<span class="agent-company"><?php the_field('agent_company'); ?></span>
					<span class="agent-tagline"><?php the_field('agent_tagline'); ?></span>
				</header>
				<?php the_field('agent_description'); ?>
				<a href="#" data-client-name="<?php echo $title; ?>" data-agent-name="<?php the_title(); ?>" class="contact-me-button button">
					<span>Contact Me</span>
					<div class="loader">
					  <svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					   width="40px" height="40px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
					  <path opacity="0.2" fill="#000" d="M20.201,5.169c-8.254,0-14.946,6.692-14.946,14.946c0,8.255,6.692,14.946,14.946,14.946
					    s14.946-6.691,14.946-14.946C35.146,11.861,28.455,5.169,20.201,5.169z M20.201,31.749c-6.425,0-11.634-5.208-11.634-11.634
					    c0-6.425,5.209-11.634,11.634-11.634c6.425,0,11.633,5.209,11.633,11.634C31.834,26.541,26.626,31.749,20.201,31.749z"/>
					  <path fill="#000" d="M26.013,10.047l1.654-2.866c-2.198-1.272-4.743-2.012-7.466-2.012h0v3.312h0
					    C22.32,8.481,24.301,9.057,26.013,10.047z">
					    <animateTransform attributeType="xml"
					      attributeName="transform"
					      type="rotate"
					      from="0 20 20"
					      to="360 20 20"
					      dur="0.5s"
					      repeatCount="indefinite"/>
					    </path>
					  </svg>
					</div>
					<div class="success">
						<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 26" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 26 26">
						  <path d="m.3,14c-0.2-0.2-0.3-0.5-0.3-0.7s0.1-0.5 0.3-0.7l1.4-1.4c0.4-0.4 1-0.4 1.4,0l.1,.1 5.5,5.9c0.2,0.2 0.5,0.2 0.7,0l13.4-13.9h0.1v-8.88178e-16c0.4-0.4 1-0.4 1.4,0l1.4,1.4c0.4,0.4 0.4,1 0,1.4l0,0-16,16.6c-0.2,0.2-0.4,0.3-0.7,0.3-0.3,0-0.5-0.1-0.7-0.3l-7.8-8.4-.2-.3z"/>
						</svg>
					</div>
				</a>
			</div>
			<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

		<?php endif; ?>

		<?php

		$post_object = get_field('agent_number_2');

		$title = get_the_title();

		if( $post_object ):

			// override $post
			$post = $post_object;
			setup_postdata( $post );

			?>
			<div class="agent" id="agent_2">
				<figure>
					<?php if( get_field('agent_photo') ): ?>
						<img src="<?php the_field('agent_photo'); ?>" alt="" />
					<?php else : ?>
						<img src="<?php echo get_template_directory_uri(); ?>/media/agent-placeholder.png" alt="Agent" />
					<?php endif; ?>
				</figure>
				<header>
					<h3><?php the_title(); ?></h3>
					<span><?php the_field('agent_company'); ?></span>
					<span><?php the_field('agent_tagline'); ?></span>
				</header>
				<?php the_field('agent_description'); ?>
				<a href="#" data-client-name="<?php echo $title; ?>" data-agent-name="<?php the_title(); ?>" class="contact-me-button button">
					<span>Contact Me</span>
					<div class="loader">
					  <svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					   width="40px" height="40px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
					  <path opacity="0.2" fill="#000" d="M20.201,5.169c-8.254,0-14.946,6.692-14.946,14.946c0,8.255,6.692,14.946,14.946,14.946
					    s14.946-6.691,14.946-14.946C35.146,11.861,28.455,5.169,20.201,5.169z M20.201,31.749c-6.425,0-11.634-5.208-11.634-11.634
					    c0-6.425,5.209-11.634,11.634-11.634c6.425,0,11.633,5.209,11.633,11.634C31.834,26.541,26.626,31.749,20.201,31.749z"/>
					  <path fill="#000" d="M26.013,10.047l1.654-2.866c-2.198-1.272-4.743-2.012-7.466-2.012h0v3.312h0
					    C22.32,8.481,24.301,9.057,26.013,10.047z">
					    <animateTransform attributeType="xml"
					      attributeName="transform"
					      type="rotate"
					      from="0 20 20"
					      to="360 20 20"
					      dur="0.5s"
					      repeatCount="indefinite"/>
					    </path>
					  </svg>
					</div>
					<div class="success">
						<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 26" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 26 26">
						  <path d="m.3,14c-0.2-0.2-0.3-0.5-0.3-0.7s0.1-0.5 0.3-0.7l1.4-1.4c0.4-0.4 1-0.4 1.4,0l.1,.1 5.5,5.9c0.2,0.2 0.5,0.2 0.7,0l13.4-13.9h0.1v-8.88178e-16c0.4-0.4 1-0.4 1.4,0l1.4,1.4c0.4,0.4 0.4,1 0,1.4l0,0-16,16.6c-0.2,0.2-0.4,0.3-0.7,0.3-0.3,0-0.5-0.1-0.7-0.3l-7.8-8.4-.2-.3z"/>
						</svg>
					</div>
				</a>
			</div>
			<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

		<?php endif; ?>

		<?php

		$post_object = get_field('agent_number_3');

		$title = get_the_title();

		if( $post_object ):

			// override $post
			$post = $post_object;
			setup_postdata( $post );

			?>
			<div class="agent" id="agent_3">
				<figure>
					<?php if( get_field('agent_photo') ): ?>
						<img src="<?php the_field('agent_photo'); ?>" alt="" />
					<?php else : ?>
						<img src="<?php echo get_template_directory_uri(); ?>/media/agent-placeholder.png" alt="Agent" />
					<?php endif; ?>
				</figure>
				<header>
					<h3><?php the_title(); ?></h3>
					<span><?php the_field('agent_company'); ?></span>
					<span><?php the_field('agent_tagline'); ?></span>
				</header>
				<?php the_field('agent_description'); ?>
				<a href="#" data-client-name="<?php echo $title; ?>" data-agent-name="<?php the_title(); ?>" class="contact-me-button button">
					<span>Contact Me</span>
					<div class="loader">
					  <svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					   width="40px" height="40px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
					  <path opacity="0.2" fill="#000" d="M20.201,5.169c-8.254,0-14.946,6.692-14.946,14.946c0,8.255,6.692,14.946,14.946,14.946
					    s14.946-6.691,14.946-14.946C35.146,11.861,28.455,5.169,20.201,5.169z M20.201,31.749c-6.425,0-11.634-5.208-11.634-11.634
					    c0-6.425,5.209-11.634,11.634-11.634c6.425,0,11.633,5.209,11.633,11.634C31.834,26.541,26.626,31.749,20.201,31.749z"/>
					  <path fill="#000" d="M26.013,10.047l1.654-2.866c-2.198-1.272-4.743-2.012-7.466-2.012h0v3.312h0
					    C22.32,8.481,24.301,9.057,26.013,10.047z">
					    <animateTransform attributeType="xml"
					      attributeName="transform"
					      type="rotate"
					      from="0 20 20"
					      to="360 20 20"
					      dur="0.5s"
					      repeatCount="indefinite"/>
					    </path>
					  </svg>
					</div>
					<div class="success">
						<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 26" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 26 26">
						  <path d="m.3,14c-0.2-0.2-0.3-0.5-0.3-0.7s0.1-0.5 0.3-0.7l1.4-1.4c0.4-0.4 1-0.4 1.4,0l.1,.1 5.5,5.9c0.2,0.2 0.5,0.2 0.7,0l13.4-13.9h0.1v-8.88178e-16c0.4-0.4 1-0.4 1.4,0l1.4,1.4c0.4,0.4 0.4,1 0,1.4l0,0-16,16.6c-0.2,0.2-0.4,0.3-0.7,0.3-0.3,0-0.5-0.1-0.7-0.3l-7.8-8.4-.2-.3z"/>
						</svg>
					</div>
				</a>
			</div>
			<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

		<?php endif; ?>

	</div>
</section>

<?php get_footer(); ?>
